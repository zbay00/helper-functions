## @package configs
# helpers/configs is a lightweight yaml parser that can be used in
# projects to help define classes based on user generate {fid}.yaml files.
#
# This is a collection of helper classes to be used by various microservices within
# the various services.
#
#
# @section auther_doxygen Author(s) & Maintainer(s)
# Zack Bayhan

## Documentation configReader.
# @class configReader():
# @file configReader.py
#
# The configReader class is used to handle basic yaml configuration based Files
# for various microservice based application writen in python
#

import yaml

def __init__(self, fid):
## @Method __init__(self, fid)
# The constructor.
# @param self
# @param fid
# fid is the fileid to a specific configuration file in the yaml formate

    self.config = self.readConfig(fid)

def readConfig(self, fid):
## @Method readConfig(self, fid)
# The readConfig Method opens a yaml file and returns the stream
# @param self the object pointer
# @param fid the file id
# @return an instance of readConfig
    with open(fid, 'r') as stream:
# try catch block
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
