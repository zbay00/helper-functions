#internal imports
from helpers.datastreams.producers import kafkaProducer
from helpers.configs import configReader

import json

class streamingException(kafkaProducer):
    """docstring for .streamingException"""

    def __init__(self, fid):
        self.config = configReader(fid)
        self.greatings = self.config.config["GENERAL_KAFKA_INFO"]["GREETINGS"]
        self.bootstrap_servers = self.config.config["GENERAL_KAFKA_INFO"]["BOOTSTRAP_SERVERS"]
        self.topic = self.config.config["EXCEPTION_PRODUCER"]["TOPIC"]
        super().__init__()
        self.producer = super().configProducer()

    def produceExecptionMessage(self, message):
        super().produceMessage(json.dumps({"System-Error": message}))
