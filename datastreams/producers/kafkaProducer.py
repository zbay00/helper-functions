from helpers.configs import configReader
from confluent_kafka import Producer

import json

class kafkaProducer():
    def __init__(self):
        print("KafkaProducer.__ini__(self)")
        #self.exception = streamingException(fid)

    def configProducer(self):
        return Producer({'bootstrap.servers': self.bootstrap_servers})

    def delivery_report(self, err, msg):
        """ Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush(). """
        if err is not None:
            print('Message delivery failed: {}'.format(err))
        else:
            print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))

    def produceMessage(self, message):
        print(self.greatings.format(self.bootstrap_servers, self.topic, message))
        try:
            self.producer.produce(self.topic, json.dumps(message), callback=self.delivery_report)
        except Exception as e:
            self.exception.printExecption(e)
            print(e)
            pass
